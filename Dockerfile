FROM cypress/included:3.2.0
WORKDIR /data
RUN git clone https://github.com/tastejs/todomvc.git /data/todomvc
WORKDIR /data/todomvc
RUN cd /data/todomvc
RUN rm package-lock.json
RUN npm install
ADD . .
EXPOSE 8000
#CMD ["npm","run test-server"]
