
it('creates snapshot', () => {
	cy.visit('http://localhost:8000');

	cy.get('.container').toMatchImageSnapshot({
		threshold: 0.001,
	});

});
